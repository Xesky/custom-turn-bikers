﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Card {

    CardType cardType;
    int value;

    public Card (CardType cardType, int value)
    {
        this.cardType = cardType;
        this.value = value;
    }

    public CardType GetCardType ()
    {
        return cardType;
    }

    public int GetValue()
    {
        return value;
    }
}
