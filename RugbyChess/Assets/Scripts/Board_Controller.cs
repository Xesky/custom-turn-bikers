﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Board_Controller : NetworkBehaviour {

    public GameObject chipPlayerOne;
    public GameObject chipPlayerTwo;
    public int chipsPerPlayer;
    public int columns;

    public GameObject optionSprite;
    public GameObject selectedChipSprite;

    Chip[,] board;
    Chip[] playerOneChips;
    Chip[] playerTwoChips;

    List<GameObject> possibleOptions;

    // Use this for initialization
    public override void OnStartServer() {
        board = new Chip[columns, chipsPerPlayer];
        playerOneChips = new Chip[chipsPerPlayer];
        playerTwoChips = new Chip[chipsPerPlayer];
        possibleOptions = new List<GameObject>();
    }

    public void InitBoard ()
    {
        for (int i = 0; i < chipsPerPlayer; i++)
        {
            // Player One
            GameObject chipObject = Instantiate(chipPlayerOne, new Vector3(0.5f, i + 0.5f, 0), Quaternion.identity);
            Chip chip = chipObject.GetComponent<Chip>();
            chip.SetAttributes(new Vector2(0, i), Player.PlayerOne);
            board[0, i] = chip;
            playerOneChips[i] = chip;
            NetworkServer.Spawn(chipObject);

            // Player Two
            chipObject = Instantiate(chipPlayerTwo, new Vector3(columns - 1 + 0.5f, i + 0.5f, 0), Quaternion.identity);
            chip = chipObject.GetComponent<Chip>();
            chip.SetAttributes(new Vector2(columns - 1, i), Player.PlayerTwo);
            board[columns - 1, i] = chip;
            playerTwoChips[i] = chip;
            NetworkServer.Spawn(chipObject);
        }
    }

    public void InstantiatePossiblePositions (List<Vector2> possiblePositions)
    {
        possibleOptions.Clear();
        foreach (Vector2 position in possiblePositions)
        {
            if (position.x >= 0 && position.y >= 0 && position.x < columns && position.y < chipsPerPlayer && board[(int) position.x, (int) position.y] == null)
            {
                GameObject option = Instantiate(optionSprite, new Vector3(position.x + 0.5f, position.y + 0.5f, 0), Quaternion.identity);
                PossibleOption possibleOption = option.GetComponent<PossibleOption>();
                possibleOption.SetPosition(new Vector2(position.x, position.y));
                //possibleOptions.Add(option);
            }
        }
    }

    void ChangeChipBoardPosition (Vector2 origin, Vector2 destination)
    {
        board[(int)destination.x, (int)destination.y] = board[(int)origin.x, (int)origin.y];
        board[(int) origin.x, (int) origin.y] = null;

    }

    // TEMPORALLY OUT OF SERVICE
    public void ChipSelected (Chip selectedChip)
    {
        if (selectedChip.GetPlayer() == Player.PlayerOne)
        {
            int greaterX = 0;
            int lowerX = columns - 1;
            foreach (Chip chip in playerOneChips)
            {
                Vector2 position = chip.GetPosition();
                if ((int) position.x > greaterX)
                {
                    greaterX = (int) position.x;
                }
                if ((int) position.x < lowerX)
                {
                    lowerX = (int) position.x;
                }
            }

            if (greaterX == lowerX || (int) selectedChip.GetPosition().x < greaterX)
            {
                // Allow movement
                //MoveChip(selectedChip, selectedChip.GetPosition());
            }

        }
        else
        {
            int greaterX = columns - 1;
            int lowerX = 0;
            foreach (Chip chip in playerTwoChips)
            {
                Vector2 position = chip.GetPosition();
                if ((int)position.x < greaterX)
                {
                    greaterX = (int) position.x;
                }
                if ((int)position.x > lowerX)
                {
                    lowerX = (int) position.x;
                }
            }

            if (greaterX == lowerX || (int)selectedChip.GetPosition().x > greaterX)
            {
                // Allow movement
                //MoveChip(selectedChip, selectedChip.GetPosition());
            }
        }
    }

    public void MoveChip (Vector2 destination)
    {
        Chip chip = Game_Controller.instance.GetSelectedChip();
        Destroy(GameObject.FindWithTag(Game_Controller.TAG_SELECTED_CHIP));
        Game_Controller.instance.DestroyGameObjectsWithTag(Game_Controller.TAG_POSSIBLE_OPTION);

        chip.StartCoroutine(chip.ChangeChipVisualPosition(2f, destination));
        ChangeChipBoardPosition(chip.GetPosition(), destination);
    }

    public void SetSelectedChip (Chip chip)
    {
        Destroy(GameObject.FindWithTag(Game_Controller.TAG_SELECTED_CHIP));
        Vector2 position = chip.GetPosition();
        Instantiate(selectedChipSprite, new Vector3(position.x + 0.5f, position.y + 0.5f, 0), Quaternion.identity);
        Game_Controller.instance.SetSelectedChip(chip);
    }
}
