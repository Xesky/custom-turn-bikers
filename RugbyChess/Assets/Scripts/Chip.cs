﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Chip : NetworkBehaviour {

    Vector2 position;
    Player player;

    public void SetAttributes (Vector2 position, Player player)
    {
        this.position = position;
        this.player = player;
    }

    public Vector2 GetPosition ()
    {
        return position;
    }

    public void SetPosition(Vector2 position)
    {
        this.position = position;
    }

    public Player GetPlayer()
    {
        return player;
    }

    void OnMouseDown()
    {
        //Debug.Log("Chip [" + position.x + ", " + position.y + "]");
        Board_Controller board_Controller = GameObject.Find("GameManager").GetComponent<Board_Controller>();
        board_Controller.SetSelectedChip(this);
    }

    public IEnumerator ChangeChipVisualPosition(float time, Vector2 newPosition)
    {
        float counter = 0f;
        while (counter <= time)
        {
            counter = counter + Time.deltaTime;
            float percent = Mathf.Clamp01(counter / time);

            transform.position = Vector2.Lerp(position + new Vector2(0.5f, 0.5f), newPosition + new Vector2(0.5f, 0.5f), percent);

            yield return null;
        }

        position = newPosition;
    }
}
