﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PossibleOption : MonoBehaviour {

    Vector2 position;



    public void SetPosition (Vector2 position)
    {
        this.position = position;
    }

    void OnMouseDown()
    {
        //Debug.Log("PossibleOption [" + position.x + ", " + position.y + "]");
        GameObject gameManager = GameObject.Find("GameManager");
        Board_Controller board_Controller = gameManager.GetComponent<Board_Controller>();
        Cards_Controller cards_Controller = gameManager.GetComponent<Cards_Controller>();

        board_Controller.MoveChip(position);

        UI_Manager.instance.StartCoroutine(UI_Manager.instance.ConsumeCard());
        Game_Controller.instance.FinishTurn();
    }
}
