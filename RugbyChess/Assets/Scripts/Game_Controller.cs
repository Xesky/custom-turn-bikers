﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public enum Player
{
    PlayerOne,
    PlayerTwo,
    Spectator
}

public class Game_Controller : NetworkBehaviour {

    public static readonly string TAG_POSSIBLE_OPTION = "PossibleOption";
    public static readonly string TAG_SELECTED_CHIP = "SelectedChip";

    public static Game_Controller instance;

    Board_Controller boardController;
    Cards_Controller cardsController;

    [SyncVar] string playerOneName = null;
    [SyncVar] string playerTwoName = null;
    NetworkConnection connectionPlayerOne;
    NetworkConnection connectionPlayerTwo;

    Card selectedCardPlayerOne = null;
    Card selectedCardPlayerTwo = null;
    Chip selectedChipPlayerOne = null;
    Chip selectedChipPlayerTwo = null;

    [SyncVar]
    Player playerTurn;

    private void Awake()
    {
        if (instance != null)
        {
            return;
        }
        instance = this;
    }

    // Use this for initialization
    void Start () {
        // Start first turn
        playerTurn = Player.PlayerOne;

        if (!isServer)
        {
            return;
        }

        Debug.Log("I AM THE SERVER");

        boardController = gameObject.GetComponent<Board_Controller>();
        boardController.InitBoard();
        cardsController = gameObject.GetComponent<Cards_Controller>();
        
    }

    void Update()
    {
        if (playerOneName != null && playerTwoName != null)
        {
            cardsController.InitCards();
        }
    }

    public void SetSelectedCard (Card card)
    {
        if (playerTurn == Player.PlayerOne)
        {
            selectedCardPlayerOne = card;
        }
        else
        {
            selectedCardPlayerTwo = card;
        }
        
        CalculateOptions();
    }

    public void SetSelectedChip(Chip chip)
    {
        if (playerTurn == Player.PlayerOne)
        {
            selectedChipPlayerOne = chip;
        }
        else
        {
            selectedChipPlayerTwo = chip;
        }
        CalculateOptions();
    }

    public Chip GetSelectedChip()
    {
        if (playerTurn == Player.PlayerOne)
        {
            return selectedChipPlayerOne;
        }
        else
        {
            return selectedChipPlayerTwo;
        }
    }

    public void SetPlayerName(string uniqueName)
    {
        if (playerOneName == null)
        {
            //Debug.Log("Player ONE unique name: " + uniqueName);
            playerOneName = uniqueName;
            connectionPlayerOne = GameObject.Find(playerOneName).GetComponent<NetworkIdentity>().connectionToClient;
            TargetDisableEnemyInteraction(connectionPlayerOne, Player.PlayerOne);
        }
        else if (playerTwoName == null)
        {
            //Debug.Log("Player TWO unique name: " + uniqueName);
            playerTwoName = uniqueName;
            connectionPlayerTwo = GameObject.Find(playerTwoName).GetComponent<NetworkIdentity>().connectionToClient;
            TargetDisableEnemyInteraction(connectionPlayerTwo, Player.PlayerTwo);
            TargetDisableOwnCards(connectionPlayerTwo, Player.PlayerTwo);
        }
    }

    [TargetRpc]
    void TargetDisableEnemyInteraction (NetworkConnection target, Player player)
    {
        if (player == Player.PlayerOne)
        {
            UI_Manager.instance.DisableButtonsPlayerTwo();
        }
        else
        {
            UI_Manager.instance.DisableButtonsPlayerOne();
        }
    }

    public Player GetPlayerTurn()
    {
        return playerTurn;
    }

    void CalculateOptions ()
    {
        DestroyGameObjectsWithTag(TAG_POSSIBLE_OPTION);

        Card selectedCard;
        Chip selectedChip;
        if (playerTurn == Player.PlayerOne)
        {
            selectedCard = selectedCardPlayerOne;
            selectedChip = selectedChipPlayerOne;
        }
        else
        {
            selectedCard = selectedCardPlayerTwo;
            selectedChip = selectedChipPlayerTwo;
        }


        if (selectedCard == null || selectedChip == null)
        {
            return;
        }

        Vector2 position = selectedChip.GetPosition();
        int x = (int) position.x;
        int y = (int) position.y;
        int value = selectedCard.GetValue();

        List<Vector2> possiblePositions = new List<Vector2>();

        switch (selectedCard.GetCardType())
        {
            case CardType.Horizontal:
                possiblePositions.Add(new Vector2(x + value, y));
                possiblePositions.Add(new Vector2(x - value, y));
                break;
            case CardType.Vertical:
                possiblePositions.Add(new Vector2(x, y + value));
                possiblePositions.Add(new Vector2(x, y - value));
                break;
            case CardType.Ascendant:
                possiblePositions.Add(new Vector2(x + value, y + value));
                possiblePositions.Add(new Vector2(x - value, y - value));
                break;
            case CardType.Descendant:
                possiblePositions.Add(new Vector2(x + value, y - value));
                possiblePositions.Add(new Vector2(x - value, y + value));
                break;
        }

        boardController.InstantiatePossiblePositions(possiblePositions);
    }

    public void DestroyGameObjectsWithTag (string tag)
    {
        GameObject[] gameObjects;
        gameObjects = GameObject.FindGameObjectsWithTag(tag);

        foreach (GameObject go in gameObjects)
        {
            Destroy(go);
        }
    }

    public void FinishTurn ()
    {
        if (playerTurn == Player.PlayerOne)    // Empieza el turno del jugador dos
        {
            playerTurn = Player.PlayerTwo;
            selectedCardPlayerOne = null;
            selectedChipPlayerOne = null;
            // Desactivar chips jugador uno
            // Desactivar cartas jugador uno
            TargetDisableOwnCards(connectionPlayerOne, Player.PlayerOne);
            // Activar chips jugador dos
            // Activar cartas jugador dos
            TargetEnableOwnCards(connectionPlayerTwo, Player.PlayerTwo);

            // Robar carta jugador dos
            Card card = cardsController.DrawNewCard(Player.PlayerTwo);
            UI_Manager.instance.StartCoroutine(UI_Manager.instance.DrawCard(card, Player.PlayerTwo));
        }
        else    // Empieza el turno del jugador uno
        {
            playerTurn = Player.PlayerOne;
            selectedCardPlayerTwo = null;
            selectedChipPlayerTwo = null;
            // Desactivar chips jugador dos
            // Desactivar cartas jugador dos
            TargetDisableOwnCards(connectionPlayerTwo, Player.PlayerTwo);
            // Activar chips jugador uno
            // Activar cartas jugador uno
            TargetEnableOwnCards(connectionPlayerOne, Player.PlayerOne);

            // Robar carta jugador uno
            Card card = cardsController.DrawNewCard(Player.PlayerOne);
            UI_Manager.instance.StartCoroutine(UI_Manager.instance.DrawCard(card, Player.PlayerOne));
        }
    }

    [TargetRpc]
    void TargetDisableOwnCards(NetworkConnection target, Player player)
    {
        if (player == Player.PlayerOne)
        {
            UI_Manager.instance.DisableButtonsPlayerOne();
        }
        else
        {
            UI_Manager.instance.DisableButtonsPlayerTwo();
        }
    }

    [TargetRpc]
    void TargetEnableOwnCards(NetworkConnection target, Player player)
    {
        if (player == Player.PlayerOne)
        {
            UI_Manager.instance.EnableButtonsPlayerOne();
        }
        else
        {
            UI_Manager.instance.EnableButtonsPlayerTwo();
        }
    }
}
