﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class UI_Manager : NetworkBehaviour {

    public static UI_Manager instance;

    public float cardsSpeed;
    public List<GameObject> playerOneCards;
    public List<GameObject> playerTwoCards;
    public List<Sprite> cardSprites;

    Cards_Controller cardsController;

    int emptyCardPlayerOne = 0;
    int emptyCardPlayerTwo = 0;

    void Awake()
    {
        if (instance != null)
        {
            return;
        }
        instance = this;
    }

    void Start()
    {
        cardsController = GameObject.Find("GameManager").GetComponent<Cards_Controller>();
    }

    public IEnumerator DrawHand (Card[] cards, Player player)
    {
        foreach (Card card in cards)
        {
            StartCoroutine(DrawCard(card, player));
            yield return new WaitForSeconds(0.45f);
        }
    }

    public IEnumerator DrawCard(Card card, Player player)
    {
        GameObject cardButton;

        if (player == Player.PlayerOne)
        {
            cardButton = playerOneCards[emptyCardPlayerOne];
            emptyCardPlayerOne++;
        }
        else
        {
            cardButton = playerTwoCards[emptyCardPlayerTwo];
            emptyCardPlayerTwo++;
        }
        
        Sprite sprite = GetCardSprite(card);

        Vector2 origin = new Vector2(cardButton.transform.position.x, Screen.height);
        Vector2 destination = cardButton.transform.position;

        cardButton.transform.position = origin;
        cardButton.GetComponent<Image>().sprite = sprite;
        cardButton.GetComponent<Image>().enabled = true;

        float step = (cardsSpeed / (origin - destination).magnitude) * Time.fixedDeltaTime;
        float t = 0;
        while (t <= 1.0f)
        {
            t += step; // Goes from 0 to 1, incrementing by step each time
            cardButton.transform.position = Vector3.Lerp(origin, destination, t); // Move objectToMove closer to b
            yield return new WaitForFixedUpdate();         // Leave the routine and return here in the next frame
        }

        //cardButton.transform.localScale = Vector2.one;
        
    }

    public IEnumerator ConsumeCard()
    {
        GameObject button;
        if (Game_Controller.instance.GetPlayerTurn() == Player.PlayerOne)
        {
            button = playerOneCards[emptyCardPlayerOne];
        }
        else
        {
            button = playerTwoCards[emptyCardPlayerTwo];
        }

        float counter = 0;
        float t, time = 0.5f;
        while (counter < time)
        {
            counter += Time.deltaTime;
            t = counter / time;
            t = 1f - Mathf.Cos(t * Mathf.PI * 0.5f);
            //t = Mathf.Sin(t * Mathf.PI * 0.5f);
            button.transform.localScale = Vector3.Lerp(button.transform.localScale, Vector3.zero, t);
            yield return null;
        }
        button.GetComponent<Image>().enabled = false;
        button.transform.localScale = Vector3.one;
    }

    Sprite GetCardSprite(Card card)
    {
        int offset = 0;

        if (card.GetValue() == 2)
        {
            offset = 4;
        }

        int index = 0;
        switch (card.GetCardType())
        {
            case CardType.Vertical:
                index = 1;
                break;
            case CardType.Ascendant:
                index = 2;
                break;
            case CardType.Descendant:
                index = 3;
                break;
        }
        return cardSprites[index + offset];
    }

    public void SetSelectedCard(int pos)
    {
        if (Game_Controller.instance.GetPlayerTurn() == Player.PlayerOne)
        {
            emptyCardPlayerOne = pos;
        }
        else
        {
            emptyCardPlayerTwo = pos;
        }
        cardsController.SetSelectedCard(pos);
        
    }

    public void DisableButtonsPlayerOne()
    {
        foreach (GameObject button in playerOneCards)
        {
            button.GetComponent<Button>().interactable = false;
        }
    }

    public void EnableButtonsPlayerOne()
    {
        foreach (GameObject button in playerOneCards)
        {
            button.GetComponent<Button>().interactable = true;
        }
    }

    public void DisableButtonsPlayerTwo()
    {
        foreach (GameObject button in playerTwoCards)
        {
            button.GetComponent<Button>().interactable = false;
        }
    }

    public void EnableButtonsPlayerTwo()
    {
        foreach (GameObject button in playerTwoCards)
        {
            button.GetComponent<Button>().interactable = true;
        }
    }
}
