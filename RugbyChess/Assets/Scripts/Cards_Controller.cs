﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public enum CardType
{
    Horizontal,
    Vertical,
    Ascendant,
    Descendant
}

public class Cards_Controller : NetworkBehaviour {

    public int maxCardsInHand;

    Card[] playerOneCards;
    Card[] playerTwoCards;

    int selectedCardPositionOne = -1;
    int selectedCardPositionTwo = -1;

    public void InitCards ()
    {
        playerOneCards = new Card[maxCardsInHand];
        playerTwoCards = new Card[maxCardsInHand];
        for (int i = 0; i < maxCardsInHand; i++)
        {
            playerOneCards[i] = CreateCard();
            playerTwoCards[i] = CreateCard();
        }
        RpcDrawHand();
    }
    
    [ClientRpc]
    void RpcDrawHand()
    {
        UI_Manager.instance.StartCoroutine(UI_Manager.instance.DrawHand(playerOneCards, Player.PlayerOne));
        UI_Manager.instance.StartCoroutine(UI_Manager.instance.DrawHand(playerTwoCards, Player.PlayerTwo));
    }

    Card CreateCard ()
    {
        int value = 0;
        CardType cardType;
        int numRandom = Random.Range(0, 5);
        if (numRandom == 0)
        {
            value = 2;
        }
        else
        {
            value = 1;
        }

        numRandom = Random.Range(0, 4);
        //Debug.Log("Random num card: " + numRandom);
        switch (numRandom)
        {
            case 0:
                cardType = CardType.Horizontal;
                break;
            case 1:
                cardType = CardType.Vertical;
                break;
            case 2:
                cardType = CardType.Ascendant;
                break;
            case 3:
                cardType = CardType.Descendant;
                break;
            default:
                cardType = CardType.Descendant;
                break;
        }

        return new Card(cardType, value);
    }

    public Card DrawNewCard (Player player)
    {
        Card card = CreateCard();
        if (player == Player.PlayerOne)
        {
            playerOneCards[selectedCardPositionOne] = card;
        }
        else
        {
            playerTwoCards[selectedCardPositionTwo] = card;
        }

        return card;
    }

    public void SetSelectedCard (int pos)
    {
        if (Game_Controller.instance.GetPlayerTurn() == Player.PlayerOne)
        {
            selectedCardPositionOne = pos;
            Game_Controller.instance.SetSelectedCard(playerOneCards[pos]);
        }
        else
        {
            selectedCardPositionTwo = pos;
            Game_Controller.instance.SetSelectedCard(playerTwoCards[pos]);
        }
    }
}
