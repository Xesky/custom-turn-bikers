﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Player_ID : NetworkBehaviour {

    public bool isLocal = false;
    [SyncVar] private string playerUniqueName;
    private NetworkInstanceId playerNetID;
    private Transform myTransform;

    public override void OnStartLocalPlayer()
    {
        isLocal = true;
        GetNetIdentity();
        SetName();
    }

    void Awake()
    {
        myTransform = transform;
    }

    // Update is called once per frame
    void Update()
    {
        if (myTransform.name == "" || myTransform.name == "Player(Clone)")
        {
            SetName();
        }
    }

    void SetName()
    {
        if (isLocalPlayer)
        {
            myTransform.name = MakeUniqueName();
        }
        else
        {
            myTransform.name = playerUniqueName;
        }
    }

    // Only executed on local
    string MakeUniqueName()
    {
        return "Player " + playerNetID.ToString();
    }

    [Client]
    void GetNetIdentity()
    {
        playerNetID = GetComponent<NetworkIdentity>().netId;
        CmdTellServerMyName(MakeUniqueName());
    }

    [Command]
    void CmdTellServerMyName(string name)
    {
        playerUniqueName = name;
        StartCoroutine(WaitSeconds(1f));
    }

    public IEnumerator WaitSeconds(float seconds)
    {
        yield return new WaitForSeconds(seconds);
        Game_Controller.instance.SetPlayerName(playerUniqueName);
    }
}
